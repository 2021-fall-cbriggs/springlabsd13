package com.oreillyauto.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oreillyauto.dao.ExampleRepository;
import com.oreillyauto.domain.examples.Example;
import com.oreillyauto.service.ExampleService;

@Service
public class ExampleServiceImpl implements ExampleService {
    
    @Autowired
    ExampleRepository exampleRepository;

    @Override
    public List<Example> getExamples() {
        return exampleRepository.getExamples();
    }

    @Override
    public List<Example> getExampleById(Integer id) {  
         return exampleRepository.getExampleById(id);
    }

	@Override
	@Transactional
	public void testQueries(String day) {
		exampleRepository.testQueries(day);	
	}
    
}
