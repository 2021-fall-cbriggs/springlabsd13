package com.oreillyauto.domain.examples;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
//import javax.validation.constraints.NotNull;

//import org.hibernate.validator.constraints.NotEmpty;

@Entity
//@Immutable <-- Do not add when you have an Auto Incremented GUID
@Table(name="NICKNAMES")
public class Nickname implements Serializable {

    private static final long serialVersionUID = -5996834027120348470L;
    
    public Nickname() {}
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)   // AUTO INCREMENTED ID !!
    @Column(name = "id", columnDefinition = "INTEGER")
    private Integer id;

    //@NotNull
    //@NotEmpty
    @Column(name = "nick_name", columnDefinition = "VARCHAR(32)")
    private String nickName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((nickName == null) ? 0 : nickName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Nickname other = (Nickname) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (nickName == null) {
            if (other.nickName != null)
                return false;
        } else if (!nickName.equals(other.nickName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Nickname [id=" + id + ", nickName=" + nickName + "]";
    }
}
