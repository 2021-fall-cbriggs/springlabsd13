package com.oreillyauto.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.oreillyauto.domain.interns.Intern;
import com.oreillyauto.service.MappingsService;

@Controller
public class ExampleTemplateController {

	@Autowired
	MappingsService mappingService;
	
	/**
	 * I don't know that I like using a jsonTemplate configuration. The
	 * result is always an Object {} that holds the information. Instead 
	 * of returning an array of Intern Data:
	 *   [{<Intern>}, {<Intern>}]
	 * the code below returns:
	 *   { "InternList" : [{<Intern>}, {<Intern>}] }
	 * @param model
	 * @return
	 */
    @GetMapping(value = "/rest/internsTemplate")
    public String getAllInternsTemplate(Model model) {
    	List<Intern> internList = mappingService.getInterns();
    	model.addAttribute("internList", internList);
    	return "jsonTemplate";
    }
}
