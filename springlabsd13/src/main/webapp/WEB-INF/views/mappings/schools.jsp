<%@ include file="/WEB-INF/layouts/include.jsp"%>
<h1>Unidirectional Mappings</h1>
<h3>Unidirectional One-to-Many</h3>
<hr/>
<h4>Schools</h4>
<p>
	Since we have a unidirectional mapping from School to
	Student, we can query students that go to a school. We
	cannot query the school for each student without throwing
	an exception (since we are not mapping "School school" in
	the Student domain entity).
</p>
<c:forEach items="${schoolList}" var="school">
	<div><strong>* ${school}</strong></div>
	<c:forEach items="${school.studentList}" var="student">	
		<div>** ${student}</div>
	</c:forEach>
</c:forEach>

<hr/>

<h4>Students</h4>
<c:forEach items="${studentList}" var="student">
	<div>* ${student}</div>
</c:forEach>
<!-- 	We cannot query the school for each student without throwing
	an exception (since we are not mapping "School school" in
	the Student domain entity). -->
<%-- 	<div>** ${student.school}</div> --%>


<hr/>

<h4>User Tables</h4>
<c:forEach items="${userTableList}" var="table">
	${table}<br/>
</c:forEach>

<script>	
	orly.ready.then(() => {
	});
</script>
